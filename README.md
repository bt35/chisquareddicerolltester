# ChiSquaredDiceRollTester

Implementing solution provided in: https://rpg.stackexchange.com/questions/70802/how-can-i-test-whether-a-die-is-fair

# D20 at 100 Rolls

#### Hand rolled frequency count: 

```
rolls:
        [17, 4, 6, 17, 13, 8, 7, 1, 15, 11, 13, 16, 4, 19, 8, 8, 5, 3, 10, 1, 19, 13, 6, 10, 18, 10, 5, 8, 16, 1, 18, 19, 1, 5, 8, 8, 17, 17, 6, 10, 14, 3, 6, 10, 20, 19, 17, 7, 19, 4, 3, 10, 10, 6, 6, 12, 18, 9, 3, 11, 10, 3, 11, 12, 5, 3, 18, 4, 16, 12, 20, 19, 7, 14, 10, 5, 12, 19, 17, 15, 17, 5, 12, 3, 17, 18,
 9, 17, 1, 20, 12, 5, 10, 6, 20, 11, 8, 12, 10, 19]
Frequency:
        {1: 5, 2: 0, 3: 7, 4: 4, 5: 7, 6: 7, 7: 3, 8: 7, 9: 2, 10: 11, 11: 4, 12: 7, 13: 3, 14: 2, 15: 2, 16: 3, 17: 9, 18: 5, 19: 8, 20: 4}
f_exp:
        5.0
Degrees of Freedom:
        19
ChiSquared:
         29.600000000000005

```

![Hand rolled](freq.png)

#### Simulated with system random. So there is some data to compare from a computer RNG. 
```
rolls:
        [20, 8, 17, 16, 10, 14, 12, 3, 11, 13, 5, 10, 5, 17, 12, 9, 15, 8, 5, 18, 6, 4, 13, 2, 2, 6, 12, 13, 4, 10, 19, 9, 20, 18, 13, 7, 10, 6, 12, 5, 2, 3, 15, 3, 13, 9, 7, 14, 2, 6, 6, 5, 19, 9, 2, 4, 19, 7, 6, 13, 8, 6, 1, 9, 11, 13, 6, 1, 10, 17, 20, 5, 18, 15, 14, 19, 10, 4, 17, 18, 16, 19, 11, 11, 1, 15, 11
, 18, 4, 16, 10, 10, 13, 12, 15, 11, 11, 1, 10, 9]
Frequency:
        {1: 4, 2: 5, 3: 3, 4: 5, 5: 6, 6: 8, 7: 3, 8: 3, 9: 6, 10: 9, 11: 7, 12: 5, 13: 8, 14: 3, 15: 5, 16: 3, 17: 4, 18: 5, 19: 5, 20: 3}
f_exp:
        5.0
Degrees of Freedom:
        19
ChiSquared:
         13.200000000000003
```

![Simulated rolled](simulatedfreq.png)

# D6 at 

```
Hand rolled output:
rolls:
        [4, 5, 4, 3, 2, 5, 4, 3, 4, 6, 3, 6, 3, 4, 6, 6, 4, 5, 6, 1, 6, 6, 1, 2, 2, 2, 1, 1, 3, 3, 3, 4, 4, 3, 2, 4, 2, 3, 6, 6, 2, 6, 5, 6, 6, 4, 5, 2, 4, 6]
Frequency:
        {1: 4, 2: 8, 3: 9, 4: 11, 5: 5, 6: 13}
f_exp:
        8.333333333333334
Degrees of Freedom:
        5
ChiSquared:
         7.119999999999999

```
![Hand rolled](freq6.png)

```
Simulated Output:
rolls:
        [5, 5, 1, 6, 6, 3, 1, 2, 1, 4, 6, 2, 6, 6, 4, 5, 5, 6, 2, 6, 2, 4, 5, 4, 6, 2, 1, 5, 6, 4, 2, 1, 3, 1, 6, 4, 6, 5, 2, 6, 6, 2, 4, 4, 5, 6, 4, 2, 6, 4]
Frequency:
        {1: 6, 2: 9, 3: 2, 4: 10, 5: 8, 6: 15}
f_exp:
        8.333333333333334
Degrees of Freedom:
        5
ChiSquared:
         11.2

```
![Simulated rolled](simulatedfreq6.png)

### TODO: 

- Fix the d20 graph x axis numbers. 
- Clean up the code... 
