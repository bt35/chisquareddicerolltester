
import pandas as pd
import argparse
import scipy.stats as stats
import numpy as np
import secrets
import collections
import matplotlib.pyplot as plt
from collections import Counter


def args():
    parser = argparse.ArgumentParser(description='Chi Score Dice Rolls')
    parser.add_argument('-r','--rolls', dest='rolls', type=int, action='store', help='Number of rolls')
    parser.add_argument('-d','--dsides', dest='dsides', type=int, action='store', help='Number of sides')
    parser.add_argument('-s','--samples', dest='samples', type=int, action='store', help='Number of samples')
    return parser.parse_args()

def plotSave(frequency, sides, simulated):
    if not simulated:
        plt.bar(frequency.keys(), frequency.values())
        plt.ylabel("Die Sides")
        plt.xlabel("Frequency")
        plt.savefig("freq.png")
    else:
        plt.bar(frequency.keys(), frequency.values())
        plt.ylabel("Die Sides")
        plt.xlabel("Frequency")
        plt.savefig("simulatedfreq.png")

'''This seems like the correct implementation of https://rpg.stackexchange.com/questions/70802/how-can-i-test-whether-a-die-is-fair

Implementing the test seen above in python. I tried to use scipy but I do not understand why it outputs a power divergence 
instead of a chisquare. I probably didn't use it right. 

Todo, this needs cleaning and refactoring and a few d20 and d6 iteration tests to test data. 

Upload to github with plots and such!

'''
def chiscore(sides, rolls, simulated, samples):
    # degrees of freedom for die role. It is 1 minus the total sides of the die.
    ddof = sides - 1
    # Expected freequency list... this is the second row of the observations.
    fexp = (rolls / sides)
    # Generate the list above. expected freequency is rolls divided by the sides of the dice.
    # List for the die rolls, this is row one of the observations.
    results = []
    freequency = []
    xk2 = []
    chiScore = 0
    recounted = {}
    plotDict = {}

    # initilize the dict by sides... for frequency counting.
    for i in range(1,sides+1):
        plotDict[i] = 0


    # counter variable that does not reset the rolls variable. The number of rolls is a constant.
    count = rolls
    if not simulated:
        # Zeroth or 1st?
        # Samples is a hold over from a different implementation
        # I am not sure if I want to implement the ability to do multiple samples of rolls.
        # 2 samples would be 200 rolls and would require a chi_contingency test.
        for i in range (0,int(samples)):
            #print(i)
            # Get the roll input.
            # Maybe use some dice OCR to scan this stuff in? :) Todo.
            while int(count) > 0:
                results.append((int(input("Input roll result."))))
                count -= 1
                print( "Roll {}".format(count))
                print('Results build {}'.format(results))
            # Get frequency of each number rolled
            #recounted = Counter(results)
            # [1, 12, 12, 13, 11, 14, 15, 16, 20, 9, 8, 1, 2, 3, 4, 5, 6, 1, 2, 3
            # Plot dict
            for j in results:
                for k in plotDict.keys():
                    if j == k:
                        plotDict[k] = plotDict[k] + 1
            for k,v in plotDict.items():
                xk2.append((v - fexp)**2/fexp)
            chiScore = sum(xk2)
            # reset count for loop.
            count = rolls
        plotSave(plotDict, sides, 0)
        print("\nHand rolled output:")
    # Simulate some rolls for data comparison. This uses secrets RNG.
    elif simulated:
        rng = secrets.SystemRandom()
        for i in range (0, samples):
            while int(count) > 0:
                results.append(rng.randint(1,sides))
                count -= 1
            # Get frequency of each number rolled
            #recounted = Counter(results)
            # Plot dict
            for j in results:
                for k in plotDict.keys():
                    if j == k:
                        plotDict[k] = plotDict[k] + 1
            for k,v in plotDict.items():
                xk2.append((v - fexp)**2/fexp)
            chiScore = sum(xk2)
            # reset count for loop.
            count = rolls
        plotSave(plotDict, sides, 1)
        print("\nSimulated Output:")
    else:
        sys.exit(1)
    #print(results)

    print("rolls: \n\t{}".format(results))
    print("Frequency: \n\t{}".format(plotDict))
    print("f_exp: \n\t{}".format(fexp))
    print("Degrees of Freedom: \n\t{} ".format(ddof))
    print("ChiSquared:\n\t {}".format(chiScore))


    return results


def main():

    a = args()

    chiscore(a.dsides, a.rolls, 0, a.samples)
    chiscore(a.dsides, a.rolls, 1, a.samples)

main()
